# PLANTMEGA


## Introduction

This repo contains both easy project with [arduino based design project]() (the prototype) and a 2 layers [board](https://gitlab.com/terceranexus6/greenduino/-/tree/master/plantmega) ATMEGA based designed to have led, moisture sensor, battery and button. The version available is the 1st one, as detailed in [the schematic](https://gitlab.com/terceranexus6/greenduino/-/blob/master/plantmega/Schematic_plant_stuff_2020-11-12_16-25-51.pdf). Feel free to take it and modify it, as it uses [GPL v3 LICENCE](https://gitlab.com/terceranexus6/greenduino/-/blob/master/LICENCE.txt). 

After the proper testing I will launch a better version with some more details on the programming, as well as a ESP8266 hat for making it wify friendly, expect these on 2021.

## How to see the schematics and project

```
git clone <thisproject.git>
cd <thisproject>/plantmega
```

Use your favorite pcb design program, KiCad is recommended. I used easyEDA because I can't get KiCad to work in my OS.


## Prototyping


For an automated mini-greenhouse project using open hardware.

FIrst of all, using arduino and components:

Material (_already setup_):
- Arduino nano
- Soil sensor
- Arduino OLED screen

Material (_still not setup_):
- ESP8266
- Temperature sensor
- UV sensor


For the basic code:

Arduino nano + OLED + Soil moisture sensor

OLED:

![](https://simple-circuit.com/wp-content/uploads/2018/06/arduino-ssd1306-oled-spi-interface-circuit.png)

Soil moisture:

![](http://www.circuitstoday.com/wp-content/uploads/2017/03/Interface_Soil_Sensor_Arduino.png)

## The board

The first version is available [here](https://gitlab.com/terceranexus6/greenduino/-/blob/master/plantmega). More versions coming.


